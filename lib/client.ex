defmodule Spaceworld.Client do

  alias Spaceworld.GameServer.Point

  defstruct [
    id: 0,
    coordinate: Point
  ]
end
