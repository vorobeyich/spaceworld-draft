defmodule Spaceworld.GameLogic do

  alias Spaceworld.GameServer.Point
  alias Spaceworld.GameServer.WorldState

  defmodule CommandSpec do
    @callback new(client_id :: integer, name :: atom) :: Command.t
    @callback calculate(state :: map) :: map
  end

  defmodule Command do
    defstruct [ client_id: nil, action: :move, data: nil ]

    @type t :: %Command{ client_id: integer(), action: atom(), data: tuple() | none() }

    @enforce_keys [ :client_id, :action ]
  end

  @behaviour CommandSpec

  @commands_list [
    :enter,
    :move,
    :turn_left,
    :turn_right
  ]

  def new(client_id, name) do
    case name in @commands_list do
      true -> %Command{ :client_id => client_id, :action => name }
      _    -> raise "Unknown command"
    end
  end

  def new(client_id, name, data) do
    case name in @commands_list do
      true -> %Command{ :client_id => client_id, :action => name, :data => data }
      _    -> raise "Unknown command"
    end
  end

  def calculate(%WorldState{} = state, commands) when is_list(commands) do
    Enum.each(commands, fn cmd -> exec_command(state, cmd) end)
  end

  def calculate(%WorldState{} = state, []), do: state
  def calculate(%WorldState{} = state, _), do: state
  def calculate(_) do
    raise "unknown data passed to .calculate"
  end

  defp exec_command(state, %Command{:action => :enter, :data => %Point{}} = command) do
    IO.puts "CMD: #{inspect(command)}"

    state = put_in(state.clients_coordinates, [command.client_id], command.data)
  end

  defp exec_command(state, %Command{} = command) do
    raise "NOT IMPLEMENTED CMD: #{inspect(command)}"
  end

end
