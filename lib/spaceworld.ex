defmodule Spaceworld do

  alias Spaceworld.Client
  alias Spaceworld.GameServer
  alias Spaceworld.GameServer.Point

  @fps 1000

  @moduledoc """
  """

  @doc """
  lets start a world loop
  kill world after 30 ticks
  """

  def start do
    area = %Point{x: 100, y: 100}
    { :ok, game } = GameServer.start_link(area)

    client_a = %Client{id: 1, coordinate: %Point{x: 1, y: 1}}
    client_b = %Client{id: 2, coordinate: %Point{x: 10, y: 10}}

    send(game, {:client_login, client_a})
    send(game, {:client_login, client_b})
    send(game, {:get_clients_status, self()})

    receive do
      {:get_clients_status_reply, message} -> IO.puts("Reply about status is => #{inspect(message)}")
    end

    # ticker = Spaceworld.TickServer.start_link(@fps, 10, game)

    # start client supervisor

    # Spaceworld.TestEventsSender.flood(game, 10, 700)
  end

  defmodule TestEventsSender do

    def flood(pid, times, interval) when is_pid(pid) and is_number(times) and is_number(interval) do
      case times do
	num when (num > 0) ->
	  :timer.sleep(interval)
	  send(pid, {:event, {:action, :move, {1, 1}}})
	  flood(pid, times - 1, interval)
	_ ->
	  IO.puts "Flood finished"
      end
    end

  end

end
