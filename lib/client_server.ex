defmodule Spaceworld.ClientServer do

  alias Spaceworld.GameLogic

  def start_link(client, game_server) when is_map(client) and is_pid(game_server) do
    { :ok, spawn_link(__MODULE__, :init, [client, game_server]) }
  end

  def init(client, game_server) do
    loop(client, game_server)
  end

  def loop(client, game_server) do
    receive do
      {:start_callback} ->
	start_callback(client, game_server)
	loop(client, game_server)
      message ->
	IO.puts("message received by client server:\n #{ inspect(message) }")
	loop(client, game_server)
    end
  end

  defp start_callback(client, game_server) do
    IO.puts "Welcome to planet ##{inspect(client.id)}, your planned start coordinates => #{ inspect client.coordinate }"

    command = GameLogic.new(client.id, :enter, client.coordinate)
    send(game_server, {:client_command_request, command})
  end

  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: { __MODULE__, :start_link, [opts] },
      type: :worker,
      restart: :permanent,
      shutdown: 3000
    }
  end

end
