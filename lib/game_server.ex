defmodule Spaceworld.GameServer do

  alias Spaceworld.{Client, NasaSupervisor, GameLogic}
  alias Spaceworld.GameServer.{State, Point, WorldState}

  @moduledoc """
  Игровой сервер (одна планета = один сервак).

  - Отвечает за игровую логику,
  - принимает события от клиентов,
  - каждый тик просчитывает игровой мир исходя из текущего стейта + пришедших событий
  - и отправляет результаты клиентам и draw-серверу для отрисовки
  """

  defmodule Point do
    defstruct x: 0, y: 0
  end

  defmodule WorldState do
    defstruct area: Point, clients_coordinates: %{}
  end

  defmodule State do
    defstruct [
      nasa_supervisor_pid: nil,
      clients_pool: [],
      commands_buffer: [],
      world_state: %WorldState{}
    ]
  end

  def start_link(%Point{} = area) do
    world_state = %WorldState{area: area}
    state = %State{world_state: world_state}

    {:ok, nasa} = NasaSupervisor.start_link()
    state = %{state | nasa_supervisor_pid: nasa }

    {:ok, spawn_link(fn -> loop(state) end)}
  end

  def loop(%State{} = state) do

    IO.puts "c-l: #{length(state.commands_buffer)}"
    if length(state.commands_buffer) >= 2 do
      new_world_state = GameLogic.calculate(state.world_state, state.commands_buffer)
      IO.puts "new world state calculated -> #{ inspect(new_world_state) }"
    end

    receive do
      {:tick, n} ->
	loop(state)

      {:get_state, process } ->
	send(process, {:get_state_reply, state})
	loop(state)

      {:client_login, %Client{} = client} ->
	{:ok, client_pid} = NasaSupervisor.start_client(state.nasa_supervisor_pid, self(), client)
	state = update_in(state.clients_pool, fn (list) when is_list(list) -> list ++ [client] end)

	send(client_pid, {:start_callback})

	loop(state)

      {:get_clients_status, process} when is_pid(process) ->
	reply = %{ :status => Supervisor.count_children(state.nasa_supervisor_pid), :list => state.clients_pool }
	send(process, {:get_clients_status_reply, reply})

	loop(state)

      {:client_command_request, command = %GameLogic.Command{}} ->
	state = update_in(state.commands_buffer, fn (list) when is_list(list) -> list ++ [command] end)

	IO.puts "command received => #{ inspect(command) }"
	loop(state)
    end
  end

  def loop(_) do
    raise "Unknown state type"
  end

end
